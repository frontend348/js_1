function page_loaded() {
    let numberOfFilms = prompt('Сколько фильмов вы уже посмотрели?', '')
    const personalMovieDB = {
        count: numberOfFilms,
        movies: {},
        actors: {},
        genres: [],
        privat: false
    }
    let movie, rating
    for (let i = 0; i < 2; i++) {
        movie = prompt('Один из последних просмотренных фильмов?', '')
        rating = prompt('На сколько оцените его?', '')
        personalMovieDB.movies[movie] = rating
    }
    // console.log(personalMovieDB.movies)
    // строка выше выводит получившийся объект - можно раскомментировать и проверить выполнение
}

document.addEventListener('DOMContentLoaded', page_loaded)